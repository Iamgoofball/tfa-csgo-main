
Keep the same code, but use a new ent that does the damage.
Adapt the smoke to send a NW2Bool or something to extinguish the fires on ENT:Detonate (use a main ent on the fire and make it kill its childs if it gets hit on the ents find by sphere perhaps)
Study the damage (that shall be simple, a 3kliksphillip video or 5 mins on CS:GO shall do the trick)

OBSERVATIONS:
The main fire effect has a screen effect (which is a smoke) that gets in the center of the map, using a different particle effect circumvents that (I'll edit it later)

EFFECTS TO USE:

molotov_groundfire_00HIGH (Fire effect, its not a huge one, but lots of small ones)
extinguish_fire (Emitted by every extinguished fire)
molotov_explosion (Effect when the molotov explodes)
molotov_fire01 (seems to be the same as ground one, not sure)